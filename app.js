"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const HotelController = require("./controllers/hotelController");
class App {
    constructor() {
        this._hotelController = new HotelController.HotelController();
        this.express = express();
        this.routes();
    }
    routes() {
        let router = express.Router();
        router.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
        });
        router.get('/getAllHotels', (req, res, next) => {
            res.json(this._hotelController.getAll());
        });
        this.express.use('/', router);
    }
}
exports.default = new App().express;
