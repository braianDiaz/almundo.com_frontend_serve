import * as express from 'express';
import * as HotelController from './controllers/hotelController';

    class App {
      public express: express.Application;

      private  _hotelController  = new HotelController.HotelController();

      constructor() {
        this.express = express();
        this.routes();
      }


      private routes(): void {
        let router = express.Router();

        router.use(function(req, res, next) {
          res.header("Access-Control-Allow-Origin", "*");
          res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
          next();
        });
      
        router.get('/getAllHotels', (req, res, next) => {
          res.json( this._hotelController.getAll() );
        });

        this.express.use('/', router);
        
      }
    }
    export default new App().express;
