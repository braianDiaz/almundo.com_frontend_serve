"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Hotel = require("../models/hotel");
class HotelController {
    //Moq
    getAll() {
        let hotel1 = new Hotel.Hotel();
        hotel1.init({
            name: "Hotel Emperador",
            stars: 3,
            price: 1596
        });
        let hotel2 = new Hotel.Hotel();
        hotel2.init({
            name: "Petit Palace San Bernardo",
            stars: 4,
            price: 2145
        });
        let hotel3 = new Hotel.Hotel();
        hotel3.init({
            name: "Hotel Nuevo Boston",
            stars: 2,
            price: 861
        });
        let hotel4 = new Hotel.Hotel();
        hotel4.init({
            name: "Hotel Pacifico",
            stars: 4,
            price: 3500
        });
        let hotel5 = new Hotel.Hotel();
        hotel5.init({
            name: "Hotel Marlon",
            stars: 2,
            price: 2700
        });
        let hotel6 = new Hotel.Hotel();
        hotel6.init({
            name: "Hotel Nahuel",
            stars: 4,
            price: 4500
        });
        let list = [hotel1, hotel2, hotel3, hotel4, hotel5, hotel6];
        return list;
    }
}
exports.HotelController = HotelController;
