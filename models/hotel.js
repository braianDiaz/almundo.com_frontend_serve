"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Hotel {
    init(value) {
        this.name = value.name;
        this.stars = value.stars;
        this.price = value.price;
        this.img = this.getImageRandom();
    }
    getImageRandom() {
        var array = [
            "http://images.almundo.com/202/Gran_Hotel_Skorpios/1.jpg",
            "http://images.almundo.com/201/10000000/9350000/9347600/9347554/9347554_47_b.jpg",
            "http://images.almundo.com/201/13000000/12740000/12734400/12734326/97bce94a_b.jpg",
            "http://aff.bstatic.com/images/hotel/max500/800/80026517.jpg",
            "http://images.almundo.com/201/12000000/11910000/11901100/11901038/11901038_24_b.jpg",
            "http://images.almundo.com/202/hotels/Nuevo_Ostende/DSC_1453.jpg",
            "http://aff.bstatic.com/images/hotel/max500/487/48704044.jpg",
            "http://aff.bstatic.com/images/hotel/max500/106/106512367.jpg",
            "http://aff.bstatic.com/images/hotel/max500/886/88623783.jpg"
        ];
        return array[this.randomIntFromInterval(0, 4)];
    }
    randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
}
exports.Hotel = Hotel;
